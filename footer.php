<footer style="background-color: #16324F; color: white;" class="p16">
  <center><p>Hakkereille, ohjelmoijille, tietokonenörteille ja valkohatuille suunnattu sivusto.</p></center>
  <p class="p16"><b>Muista:</b> näitä ohjeita ei ole tarkoitettu pahaan, jotenka jos pystyisit esimerkiksi murtamaan koulusi sisäverkon näillä ohjeilla. Niin sinun tulee ilmoittaa siitä ATK-henkilöstölle eikä kerätä koevastauksia avoimien printtereiden kautta.</p>
  <p class="p16"><strong><center>Sivuston lähdekoodi <a href="https://github.com/kaikkitietokoneista/kaikkitietokoneista">Githubissa</a></center></strong></p>
</footer>
<script src="src/prism.js"></script>
	