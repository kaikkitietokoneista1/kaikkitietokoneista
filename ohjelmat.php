<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ohjelmat &laquo; kaikkitietokoneista.net</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="main.css">
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>

    <div class="neljäsosa oikealle">
      <iframe src="twitterembed.html" frameBorder="0" width="100%" height="500px"></iframe>
    </div>
        
    <h4 class="p16">PHP-FTP (PHP)</h4>
    <p class="p16">Yhden tiedoston ohjelma, jolla voi yhdistää ulkoiseen FTP tai FTPS-palvelimeen. Ohjelman lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/phpftp">Githubista</a>. Tehty PHP:lla.</p>

    <h4 class="p16">Kaikkidokumentit (PHP)</h4>
    <p class="p16">Yhden tiedoston ohjelma, joka tallentaa ja tekee tiedostoja MySQL-tietokantaan sisältäen yksinkertaiset rikastekstin muokkausminaisuudet. Ohjelman lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/kaikkidokumentit">Githubista</a>. Tehty PHP:lla.</p>

    <h4 class="p16">Proxymoxy (PHP)</h4>
    <p class="p16">Yhden tiedoston ohjelma, joka toimii niin sanottuna eliittiproxyna. Voit esiintyä ilman erillisten proxyien asentelua eri IP-osoitteella. Ohjelman lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/proxymoxy">Githubista</a>. Tehty PHP:lla.</p>

    <h4 class="p16">JSEdit (JavaScript)</h4>
    <p class="p16">Yhden tiedoston ohjelma (teen vain yhden tiedoston ohjelmia), joka toimii WYSIWYG-editorina selaimessa. Ohjelman lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/jsedit">Githubista</a>. Tehty JavaScript:lla.</p>
    <p class="p16">Demo: <a href="https://kaikkitietokoneista.github.io/jsedit/">kaikkitietokoneista.github.io/jsedit/</a></p>

    <h4 class="p16">JS visual (JavaScript)</h4>
    <p class="p16">JavaScript ohjelmointiympäristö, jossa ohjelmoidaan palikoilla. Perustuu <a href="https://developers.google.com/blockly">Google Blocklyyn</a>. Ohjelman lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/JS-visual">Githubista</a>. Tehty JavaScript:lla.</p>
    <p class="p16">Demo: <a href="https://kaikkitietokoneista.github.io/JS-visual/">https://kaikkitietokoneista.github.io/JS-visual/</a></p>



    <?php include 'footer.php'; ?>
  </body>
</html>
