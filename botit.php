<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Botit &laquo; kaikkitietokoneista.net</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="main.css">
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
  </head>
  <body>
    <?php include 'header.php'; ?>

    <div class="neljäsosa oikealle">
      <iframe src="twitterembed.html" frameBorder="0" width="100%" height="500px"></iframe>
    </div>

    <h4 class="p16">Sääbotti (Node.JS)</h4>
    <p class="p16">Botti ilmoittaa sinulle Telegramissa yksinkertaisella komennolla annetun kaupungin sään. Voit tavoittaa botin osoitteesta <a href="https://t.me/ktweather_bot">t.me/ktweather_bot</a>. Botin lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/weather-tgbot">Githubista</a>. Tehty Node.JS:llä</p>
    <h4 class="p16">InstantDuck (Node.JS)</h4>
    <p class="p16">DuckDuckGo:n API:n pohjautuva botti, joka antaa selityksen kysytystä asiasta englanniksi. <del>Voit tavoittaa botin osoitteesta <a href="https://t.me/instantduck_bot">t.me/instantduck_bot</a></del> (Botti ei ole päällä). Botin lähdekoodi on avoin ja löydät sen <a href="https://github.com/kaikkitietokoneista/instantduck">Githubista</a>. Tehty Node.JS:llä</p>
    <?php include 'footer.php'; ?>
  </body>
</html>
